package aiproj1;
import java.util.*;
public class Board implements Comparable<Board> {

	 private Board parent;
	 private int[] board = new int[9];
	 private double fn, hn, gn;
	
	Board()
	{
		double fn=0;
		double gn =0;
		double hn =0;
		parent = null;
	}
	Board(int newBoard[], double g, double h, Board padre)
	{
		board = newBoard;
		parent = padre;
		gn = g;
		hn = h;
		fn = g +h;
	}
	Board(int newBoard[], double g, double h)
	{
		board = newBoard;
		parent = null;
		gn = g;
		hn = h;
		fn = g +h;
	}
	void setParent(Board padre)
	{
		parent = padre;
	}
	void generate()
	{
		Random rand = new Random();
		int inversions = 0;
		do
		{
			boolean repeated[] = {false,false,false,false,false,false,false,false,false};
			int count = 0, inverseCount =0;
			while(count != 9)
			{
				int num = rand.nextInt(9);
				if(!repeated[num]){
					repeated[num] = true;
					board[count++] = num;
				}
			}
			inversions = inverseCount(board);
		} while (!isSolvable(inversions));
		
		setBoard(board);
	}
	void setBoard(int[] b)
	{
		board = b;
	}
	void setBoard(String strb)
	{
		int j = 0;
		for (int i = 0; i < 17; i=i+2)
				board[j++] = strb.charAt(i) - '0';
		setBoard(board);
	}
	boolean check(String input) //check if in correct format
	{
		boolean valid = true;
		int invCount = 0;
		boolean repeated[] = {false,false,false,false,false,false,false,false,false};
		if(input.length() != 17)
			valid = false;
		for (int i = 0; i < 17 && valid; ++i)
		{
			if(i%2==0) // if even index then it must be a number
			{
				if(!Character.isDigit(input.charAt(i)) || (input.charAt(i) -'0' < 0 && input.charAt(i) -'0' > 8) ) // if not a number or out of range
					valid = false;
				else if (repeated[input.charAt(i) -'0'])
					valid = false;
			}
			else // odd index then must be a space
				if(input.charAt(i) != ' ')
					valid = false;
		}
		if(!valid){
			System.out.println("\nError:Invalid Input");
			return false;
		}
	
		int[] boardArr=new int[9];
		int j = 0;
		for (int i = 0; i < 17; i=i+2)
			boardArr[j++] = input.charAt(i) - '0';
	
		invCount = inverseCount(boardArr);
		if(!isSolvable(invCount))
			valid = false;
	
		if(!valid){
			System.out.println("\nError:Entered puzzle not solvable");
			return false;
		}
		System.out.println("\nInput accepted");
		return true;
	}
	int inverseCount(int arr[])
	{
		int count = 0;
		for (int i = 0; i < 9-1; ++i)
			for (int j = i+1; j < 9; ++j)
				if(arr[i] != 0 && arr[j] != 0 && arr[i] > arr[j])
					count++;
		return count;
		
	}
	boolean isSolvable(int inversions)
	{
		return (inversions % 2 == 0);
	}
	
	public int compareTo(Board o) {
		if(fn == o.fn)
			return 0;
		else if (fn > o.fn)
			return 1;
		else
			return -1;
	}
	double getFn()
	{
		return fn;
	}
	double getGn()
	{
		return gn;
	}
	int[] getBoard()
	{
		return board;
	}
	int[] swap(int indexTo, int indexFrom)
	{
		int[] tempArr = board.clone();
		int temp = tempArr[indexTo];
		tempArr[indexTo] = tempArr[indexFrom];
		tempArr[indexFrom] = temp;
		return tempArr;	
	}
	Board getParent()
	{
		return parent;
	}
}
