package aiproj1;
import java.util.*;
public class Puzzle
{
	
	private PriorityQueue<Board> pQueue;
	private Vector<Board> explored = new Vector<Board>();
	Stack<Board> solution = new Stack<Board>();
	Board initBord;
	Puzzle()
	{
	Scanner keyboard = new Scanner(System.in);
	System.out.println("Welcome to the 8 puzzle game!!");
	initBord = new Board();
	String respon;
	do
	{
		System.out.println( "Menu:\n1.Generate Random Board\n2.Input custom Board\n");
		respon = keyboard.nextLine(); 
	} while (respon.equals("1") && respon.equals("2"));
	if(respon.equals("1"))
		initBord.generate();
	else if (respon.equals("2"))
		{
			do
			{
				System.out.println( "Enter puzzle sequence e.g. 1 2 3 0 4: ");
				respon = keyboard.nextLine(); 
			} while (!initBord.check(respon));
			initBord.setBoard(respon);
			initBord = new Board(initBord.getBoard(), 0.0 , 0.0);
		}
	
	autoSolveh1();
	autoSolveh2();
	System.out.println("Solved!");
	}
	void autoSolveh1()
	{
		int[] intialBoard = initBord.getBoard().clone();
		pQueue = new PriorityQueue<Board>();
		pQueue.add(new Board(initBord.getBoard(), 0.0, calcHn(initBord.getBoard(),1)));
		int emptyIndex;
		while(!isSolved(pQueue.peek()))
		{
			Board current = pQueue.poll();
			explored.add(current);
			emptyIndex = findEmpty(current.getBoard());
			Coord empty = new Coord(emptyIndex%3, emptyIndex/3); // (col, row)
			getNeighbors(empty, current, emptyIndex, 1); // adds neighbors to PQ
		} 
		Board goal = pQueue.poll();
		displaySolution(goal);
		
	}
	void autoSolveh2()
	{
		pQueue = new PriorityQueue<Board>();
		explored = new Vector<Board>();
		int[] intialBoard = initBord.getBoard().clone();
		pQueue.add(new Board(intialBoard, 0.0, calcHn(intialBoard,2)));
		int emptyIndex;
		while(!isSolved(pQueue.peek()))
		{
			Board current = pQueue.poll();
			explored.add(current);
			emptyIndex = findEmpty(current.getBoard());
			Coord empty = new Coord(emptyIndex%3, emptyIndex/3); // (col, row)
			getNeighbors(empty, current, emptyIndex, 2); // adds neighbors to PQ
		}
		Board goal =pQueue.poll();
		displaySolution(goal);	
	}
	void getNeighbors(Coord position, Board parentBoard, int emptyIndex, int whichH) 
	{
		int[] newBoard;
		double newGn = parentBoard.getGn()+1, newHn;
		if(position.x-1 > -1) // shift left
		{
			newBoard = parentBoard.swap(emptyIndex, emptyIndex - 1);
			newHn = calcHn(newBoard, whichH);
			if(!isExplored(newBoard))
				pQueue.add(new Board(newBoard, newGn, newHn, parentBoard));
		}
		if(position.x+1 < 3) // shift right
		{
			newBoard = parentBoard.swap(emptyIndex, emptyIndex + 1);
			newHn = calcHn(newBoard, whichH);
			if(!isExplored(newBoard))
				pQueue.add(new Board(newBoard, newGn, newHn, parentBoard));
		}
		if(position.y-1 > -1) // shift up
		{
			newBoard = parentBoard.swap(emptyIndex, emptyIndex - 3);
			newHn = calcHn(newBoard, whichH);
			if(!isExplored(newBoard))
				pQueue.add(new Board(newBoard, newGn, newHn, parentBoard));
		}
		if(position.y+1 <3) //shift down
		{
			newBoard = parentBoard.swap(emptyIndex, emptyIndex + 3);
			newHn = calcHn(newBoard, whichH);
			if(!isExplored(newBoard))
				pQueue.add(new Board(newBoard, newGn, newHn, parentBoard));
		}

	}
	
	void displaySolution(Board start)
	{
		Board parent = start.getParent();
		solution.push(start);
		while(parent != null) {
			solution.push(parent);
			parent = parent.getParent();
		}
		while(!solution.isEmpty())
			System.out.println(Arrays.toString(solution.pop().getBoard()));
	}
	boolean isExplored(int [] candidate)
	{
		for(int i = 0; i < explored.size(); i++)
			if(Arrays.equals(explored.get(i).getBoard(), candidate))
				return true;
		return false;
	}
	int findEmpty(int[] b)
	{
		int i = -1;
		while(b[++i] != 0);
		return i;
	}
	double calcHn(int[] board, int whichH) // missed placed tiles
	{
		int count = 0;
		if(whichH == 1) {
			if(board[8] != 0)
				count++;
			for (int i = 0; i < 8; ++i) {
				if(board[i] != i + 1)
					count++;
			}
			return count;
		}
		else
		{
			double hn =0;
			for (int i = 0; i < 8; ++i) {
				hn += manDist(board, i);
			}
			//System.out.println("h total: " + hn);
			return hn;
		}
		
	}
	double manDist(int[] board, int index) // Manhattan distance
	{
		Coord cursor = new Coord(index%3, index/3); //current coord
		int dest = board[index]-1;
		Coord finald = new Coord(dest%3, dest/3);
		return Math.abs(cursor.x - finald.x) + Math.abs(cursor.y - finald.y);
		
	}
	boolean isSolved(Board cand)
	{
		int[] check = cand.getBoard();
		for(int i = 0; i <8; i++)
			if(check[i] != i +1)
				return false;
		if(check[8] != 0)
			return false;
		return true;
	}

}